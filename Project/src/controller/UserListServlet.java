package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.Userdao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserListServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//セッションが無かったらログイン画面に飛ぶ
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			response.sendRedirect("LogingamenServlet");
			return;
		}
		//文字化け防止
		request.setCharacterEncoding("UTF-8");
		//だお発動
		Userdao userdao = new Userdao();
		ArrayList<User> list = userdao.findAll();
		//リクエストスコープ
		request.setAttribute("list", list);
		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userlist.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		//検索
		//ＪＳＰで打ったやつをリクエストパラメーター
		String searchLogin_Id = request.getParameter("login_id");
		String searchName = request.getParameter("name");
		String searchFromBirthDate = request.getParameter("fromBirthDate");
		String searchToBirthDate = request.getParameter("toBirthDate");

		//だお発動。メソッド発動。
		Userdao userdao = new Userdao();
		ArrayList<User> list = userdao.findByLoginIdOrNameOrBirthdate(searchLogin_Id, searchName, searchFromBirthDate,
				searchToBirthDate);
		request.setAttribute("list", list);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userlist.jsp");
		dispatcher.forward(request, response);

	}

}
