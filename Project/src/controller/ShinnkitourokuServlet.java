package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.Userdao;

/**
 * Servlet implementation class ShinnkitourokuServlet
 */
@WebServlet("/ShinnkitourokuServlet")
public class ShinnkitourokuServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShinnkitourokuServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			response.sendRedirect("LogingamenServlet");
			return;
		}
		RequestDispatcher dispatcher =request.getRequestDispatcher("/WEB-INF/jsp/shinnkitouroku.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得

		String NEWloginID=request.getParameter("login_id");
		String NEWPassword=request.getParameter("password");
		String PasswordCof=request.getParameter("passwordConf");
		String NEWname=request.getParameter("name");
		String NEWbrith_date=request.getParameter("birth_date");
		String NEWcreate_date=request.getParameter("create_date");
		String NEWupdate_date=request.getParameter("update_date");
		Userdao dao = new Userdao();

		//エラー各位①パス不一致　②空白　③既に登録
		if(!NEWPassword.equals(PasswordCof)){
			request.setAttribute("errmsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/shinnkitouroku.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if(NEWname.equals("")||NEWbrith_date.equals("")||NEWloginID.equals("")||NEWPassword.equals("")||PasswordCof.
				equals("")) {
			request.setAttribute("errmsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/shinnkitouroku.jsp");
			dispatcher.forward(request, response);
			return;
		}
		//③既に登録。このif文が（）内が存在してしまうときにエラーメッセ

		if(dao.findByloginId(NEWloginID)) {
			request.setAttribute("errmsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/shinnkitouroku.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//インサートメソッド発動。サーブレットに飛ぶ
		Userdao userdao = new Userdao();
		userdao.insert(NEWloginID, NEWname, NEWbrith_date, NEWPassword);

		response.sendRedirect("UserListServlet");

	}
}