package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.Userdao;
import model.User;

/**
 * Servlet implementation class UserDetailServlet
 */
@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			response.sendRedirect("LogingamenServlet");
			return;
		}


		//URLからGETパラメータとしてIDを受け取る作業。//詳細。
		String id = request.getParameter("id");
		System.out.println(id);

		//インスタンス生成、リクエストパラメーターの入力項目を引数に渡す。
		Userdao userDao = new Userdao();
		//
		User user = userDao.findById(id);
		//リクエストスコープ左はキーになる、右は実際に追加したいデータ、インスタンス形式
		request.setAttribute("user", user);
		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userdetail.jsp");
		dispatcher.forward(request, response);
	}

}
