package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.Userdao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			response.sendRedirect("LogingamenServlet");
			return;
		}

		//URLからGETパラメータとしてIDを受け取る作業。
		String id = request.getParameter("id");
		System.out.println(id);

		//初期値入力のための検索。メソッド実行のためのインスタンス生成、リクエストパラメーター生成
		Userdao userDao = new Userdao();
		User user = userDao.findById(id);

		request.setAttribute("user", user);

		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		//取得
		String id = request.getParameter("id");
		String name = request.getParameter("username");
		String password = request.getParameter("password");
		String passwordconfirm = request.getParameter("password1");
		String birthDate = request.getParameter("birth_date");
		Userdao userDao = new Userdao();
		//DAO作成→やった☑

		//?確認とパスが合わなったら・・・ユーザー情報更新画面に戻り、その際にページの下の赤色で「」を入力
		if (!password.equals(passwordconfirm)) {
			request.setAttribute("errmsg", "入力された内容は正しくありません");

			User user = userDao.findById(id);

			request.setAttribute("user", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if (name.equals("") || birthDate.equals("")) {
			request.setAttribute("errmsg", "入力された内容は正しくありません");
			User user = userDao.findById(id);

			request.setAttribute("user", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//DAOメソッド起動
		Userdao userdao = new Userdao();
		userdao.Update(id, name, password, birthDate);

		//ユーザー一覧画面に推移
		response.sendRedirect("UserListServlet");
	}

}
