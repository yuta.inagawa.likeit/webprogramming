package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.Userdao;
import model.User;
/**
 * Servlet implementation class LogingamenServlet
 */
@WebServlet("/LogingamenServlet")
public class LogingamenServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LogingamenServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") != null) {
			response.sendRedirect("UserListServlet");
			return;
		}


		RequestDispatcher dispatcher= request.getRequestDispatcher("/WEB-INF/jsp/logingamen.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String loginID=request.getParameter("login_id");
		String Password=request.getParameter("password");

		Userdao userdao = new Userdao();
		User user = userdao.findByLoginInfo(loginID,Password);

		if(user==null){
			request.setAttribute("errmsg", "ログインに失敗しました");

			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/logingamen.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//ログイン成功時セッションにユーザーの情報をセット。セッションスコープ。
		HttpSession session = request.getSession();
		session.setAttribute("userInfo", user);
		// ユーザ一覧のサーブレットにリダイレクト
		response.sendRedirect("UserListServlet");


	}

}
