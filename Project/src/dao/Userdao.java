package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class Userdao {

	//ログイン時の取り出すDAO
	public User findByLoginInfo(String login_Id, String password) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			//SQLのうち画面から入力して決める部分を ? にする
			//パラメータとして設定したい部分を?にしたSQLを使う。

			String sql = "select*from user where login_id=? and password=?";

			// SELECTを実行し、結果表を取得
			//ResultSetにはSQLの実行結果が入る。今回はrsという変数に入っている状態
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, login_Id);
			pstmt.setString(2, passwordMD5(password));

			ResultSet rs = pstmt.executeQuery();

			// ログイン失敗時の処理
			if (!rs.next()) {
				return null;
			}

			/* ログイン成功時の処理→ResultSetからカラム名を指定してレコード内のデータを取り出し
			 Beansインスタンスのフィールドにセットして返す。*/
			// 必要なデータのみインスタンスのフィールドに追加

			String loginidData = rs.getString("login_id");
			String nameData = rs.getString("name");

			return new User(loginidData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}
	//データーベースのテーブルをサーブレットに渡すセレクト文
	//一軒だはないからUSER型
	public ArrayList<User> findAll() {
		Connection con = null;
		PreparedStatement stmt = null;
		ArrayList<User> list = new ArrayList<User>();

		try {
			con = DBManager.getConnection();
			//ログインIDがadminと等しくない場合は等しくない<>を使い、それ以外を取り出す
			String sql = "SELECT * FROM user WHERE login_id <> 'admin' ";

			stmt = con.prepareStatement(sql);
			//実行
			ResultSet rs = stmt.executeQuery();
			//リストに追加する分
			while (rs.next()) {
				User beans = new User();
				beans.setId(rs.getInt("id"));
				beans.setLogin_id(rs.getString("login_id"));
				beans.setName(rs.getString("name"));
				beans.setBirth_date(rs.getDate("birth_date"));
				beans.setPassword(rs.getString("password"));
				beans.setCreate_date(rs.getString("create_date"));
				beans.setUpdate_date(rs.getString("update_date"));

				list.add(beans);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {

				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		//どこに返すか
		return list;
	}
	//新規登録するインサート文、登録はVOID。一軒だけだし。

	public void insert(String login_Id, String name, String birthdate, String password) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			con = DBManager.getConnection();

			String sql = "INSERT INTO user(login_id,name,birth_date,password,create_date,update_date) VALUES(?,?,?,?,NOW(),NOW())";

			stmt = con.prepareStatement(sql);
			//?に何を入れればいいのか。
			stmt.setString(1, login_Id);
			stmt.setString(2, name);
			stmt.setString(3, birthdate);
			stmt.setString(4, passwordMD5(password));

			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();

			}
		}

	}

	//詳細を確認するセレクト文
	public User findById(String id) {
		Connection con = null;
		// 戻り値用インスタンス生成
		User user = new User();
		try {
			con = DBManager.getConnection();

			String sql = "select * from user where id=?";

			PreparedStatement ps = con.prepareStatement(sql);

			//何を取得するか？

			ps.setInt(1, Integer.parseInt(id));

			//取得結果実行
			ResultSet rs = ps.executeQuery();
			//セットするのは…
			while (rs.next()) {
				user.setId(rs.getInt("id"));
				user.setLogin_id(rs.getString("login_id"));
				user.setName(rs.getString("name"));
				user.setBirth_date(rs.getDate("birth_date"));
				user.setCreate_date(rs.getString("create_date"));
				user.setUpdate_date(rs.getString("update_date"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return user;

	}

	//更新テーブル。
	public void Update(String id, String name, String password, String birthdate) {
		Connection con = null;

		try {
			con = DBManager.getConnection();
			//アプデ―ト文何をあぷうでーとするか。それを？にする。
			String sql = "update user SET name=?,password=?,birth_date=? WHERE id=?";
			//アプデ―と文を実行する。
			PreparedStatement stmt = con.prepareStatement(sql);
			//何をセットするのか。

			stmt.setString(1, name);
			stmt.setString(2, passwordMD5(password));
			stmt.setString(3, birthdate);
			stmt.setInt(4, Integer.parseInt(id));
			//完全実行。executeUpdate。これにてアップデート文完了。
			int result = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {

			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//デリート
	public void Delete(String id) {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DBManager.getConnection();
			//delete文
			String sql = "DELETE FROM user WHERE id=?";

			PreparedStatement stmt = con.prepareStatement(sql);

			stmt.setString(1, id);

			int result = stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {

			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//二重IDになったときにはじくようにするメソッド作り DAO
	//	public User findlogin_id(String login_id) {
	public boolean findByloginId(String login_id) {
		Connection con = null;
		PreparedStatement ps = null;
		// 登録可否フラグ（入力されたログインIDがすでにデータベースに存在していればtrue）
		boolean registAvailabilityFlg = false;

		try {
			//DB接続
			con = DBManager.getConnection();

			//セレクト文
			//			String sql = "SELECT * FROM user WHERE login_id=? ";

			//集約関数。カウント文。ASはエイリアス。カウントの（）になにを集計したいのかをセット。
			String sql = "SELECT count(login_id) AS count FROM user WHERE login_id=? ";

			// SELECTを実行し、結果表を取得
			PreparedStatement stmt = con.prepareStatement(sql);
			//?に入るやるを指定左から順に。
			stmt.setString(1, login_id);

			//結果実行
			ResultSet rs = stmt.executeQuery();

			//rsに件数がないなら
			//			if (!rs.next()) {
			//				return null;
			//			}
			//データーベースに存在。

			//結果取得。このままでは1件でも2件でも0件でも入る
			if (rs.next()) {
				//エイリアスcountが1件の場合。registAvailabilityFlgがtrueとなる。☛データベースに在るということ。
				//0件を否定→=0true
				if (rs.getInt("count") != 0) {
					registAvailabilityFlg = true;
				}
			}
			// あったら
			//			String Login_ID = rs.getString("login_id");

			//リターンをする。
			//			return new User(Login_ID);
			return registAvailabilityFlg;

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {

			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return true;
	}

	//暗号化
	public String passwordMD5(String password) {
		//ハッシュを生成したい元の文字列
		String source = password;

		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;

		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理。try文にしたほうがいい。
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);
		System.out.println(result);

		return result;
	}
	//検索
	//検索結果は配列リストになる。
	public ArrayList<User> findByLoginIdOrNameOrBirthdate(String loginId,String name, String fromBirthDate, String toBirthDate) {

		Connection con = null;
		//リスト
		ArrayList<User> list = new ArrayList<User>();
		try {

			con = DBManager.getConnection();
			//セレクトを実行し、結果表を取得
			Statement stmt = con.createStatement();

			//セレクト文。セレクトして、条件を＋でつけていく。
			String sql = "SELECT * FROM user WHERE login_id <> 'admin' ";
			// ログインIDが空でなければ条件追加
			if(!loginId.equals("")) {
				//SQL ＋＝
				sql += "AND login_id = '" + loginId + "' ";
			}

			if(!name.equals("")) {
				sql += "AND name like  '%"+name+ "%' ";
			}

			if(!fromBirthDate.equals("")) {
				sql += "AND birth_date >= '"+fromBirthDate+"' ";
			}

			if(!toBirthDate.equals("")) {
				sql += "AND birth_date <= '"+toBirthDate+"' ";
			}

			//?に入るやつを指定

			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
            // USERインスタンスに設定し、ArrayListインスタンスに追加

			while (rs.next()) {

				User beans = new User();
				beans.setId(rs.getInt("id"));
				beans.setLogin_id(rs.getString("login_id"));
				beans.setName(rs.getString("name"));
				beans.setBirth_date(rs.getDate("birth_date"));
				beans.setPassword(rs.getString("password"));
				beans.setCreate_date(rs.getString("create_date"));
				beans.setUpdate_date(rs.getString("update_date"));

				list.add(beans);
			}

			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return list;
	}
}