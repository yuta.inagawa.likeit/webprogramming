package model;

import java.io.Serializable;
import java.sql.Date;

public class User implements Serializable{
	private int id;
	private String login_id;
	private String name;
	private Date birth_date;
	private String password;
	private String create_date;
	private String update_date;
	private  String fromBirthDate;
	private String toBirthDate;
	public User() {

	}
	//暗号化用コンストラクタ



	//ログインセッションに必要なやつのコンストラクタ
	public User(String login_Id, String name) {
		this.login_id = login_Id;
		this.name = name;
	}
	//登録などに使うコンストラクタ
	public User(int id, String login_Id, String name, Date birthdate, String password,String updatedate,
			String createdate) {
		this.id = id;
		this.login_id = login_Id;
		this.name = name;
		this.birth_date = birthdate;
		this.password = password;
		this.create_date = createdate;
		this.update_date = updatedate;
	}

	//二重ログインIDメソッド用コンストラクタ
	public User(String login_Id) {
		this.login_id = login_Id;
	}


	//セッター、ゲッター
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLogin_id() {
		return login_id;
	}
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirth_date() {
		return birth_date;
	}
	public void setBirth_date(Date birth_date) {
		this.birth_date = birth_date;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCreate_date() {
		return create_date;
	}
	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}
	public String getUpdate_date() {
		return update_date;
	}
	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}



	public String getFromBirthDate() {
		return fromBirthDate;
	}



	public void setFromBirthDate(String fromBirthDate) {
		this.fromBirthDate = fromBirthDate;
	}



	public String getToBirthDate() {
		return toBirthDate;
	}



	public void setToBirthDate(String toBirthDate) {
		this.toBirthDate = toBirthDate;
	}

}
