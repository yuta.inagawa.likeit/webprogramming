<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="userlist.css">

	<link rel="stylesheet" href="login.css">
	<title>ログイン画面</title>

</head>

<body>
	<c:if test="${errmsg != null}" >
		<font color="red">${errmsg}</font>
	</c:if>

	<form class=signin action="LogingamenServlet" method="post">

	<div style="line-height: 7em">
	<h1 class="login1">
	ログイン画面<br>
	</h1>
	</div>

	<div style="line-height: 3em">
	<h2 class="login2">
	ログインID:<input type="text" name ="login_id"><br>
	パスワード:<input type="password" name= "password"><br>
	</h2>
	</div>


	<h3 class="login3">
	<input type="submit" value="ログイン">
	</h3>


</form>

</body>
</html>