<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="userlist.css">
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/bootstrap.min.css" rel="stylesheet">
<title>ユーザー情報更新</title>
<!-- BootstrapのCSS読み込み -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- オリジナルCSS読み込み -->
<link href="css/original/common.css" rel="stylesheet">
</head>
<header>
	<div class="alert alert-dark" role="alert">
		<p class="text-right">
			${sessionScope.userInfo.name}さん <a href="LogoutServlet"
				class="badge badge-logout">ログアウトする</a>
		</p>
	</div>
</header>
<c:if test="${errmsg != null}">
	<p class="text-danger"> ${errmsg}</p>
</c:if>
<body>
	<div class=container>


		<form action="UserUpdateServlet" method="post">

				<h2>
					<p class="text-center">ユーザー情報更新</p>
				</h2>

			<p class="text-left">ログインID</p>


			<p class="text-center">${user.login_id}</p>
			<br>

			<p class="text-left">パスワード</p>
			<p class=text-center>
				<input type="password" name="password">

			<p class="text-left">パスワード（確認）</p>
			<p class=text-center>
				<input type="password" name="password1">

			<p class="text-left">ユーザー名</p>
			<p class=text-center>
				<input type="text" name="username" value="${user.name}">

			<p class="text-left">生年月日</p>
			<p class=text-center>
				<input type="date" name="birth_date" value="${user.birth_date}">
				<br> <br>
				<button type="submit" class="btn btn-link">
					<h3>更新</h3>
				</button>
				<!--hiddenは画面上には表示しないでサーブレットに値を渡したい場合に利用-->
				<input type="hidden" name="id" value="${user.id}">
		</form>

		<form>
			<a href="UserListServlet" class="badge badge-back"><h4>戻る</h4></a>
		</form>
	</div>
</body>
</html>