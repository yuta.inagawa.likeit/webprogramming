<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>データ削除画面</title>
<!-- BootstrapのCSS読み込み -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- オリジナルCSS読み込み -->
<link href="css/original/common.css" rel="stylesheet">
</head>
<body>
	<header>
		<div class="alert alert-dark" role="alert">
			<p class="text-right">
				${sessionScope.userInfo.name}さん <a href="LogoutServlet" class="badge badge-logout">ログアウトする</a>
			</p>
		</div>
	</header>
	<div class="container">
		<form>
			<h2>

				<p class="text-center">
					<span class="badge badge-danger"> ユーザー削除確認 </span>
				</p>

			</h2>
		</form>
		<form>
			<br> <br>
			<p class="text-center">
				ログインID：${user.login_id} 本当に削除してよろしいでしょうか。
			</p>
			<br> <br>
		</form>
			<div class="row">
					<a class="col-sm-6" href="UserListServlet">
					 <button type="button" class="badge badge-light">いいえ</button></a>
					<form action="UserDeleteServlet" method="post">
					<a class="col-sm-6"   class="badge badge-dark">
					<input type="hidden" name="id" value="${user.id}">
					<button type="submit" class="btn btn-dark">はい</button></a>

					</form>
			</div>
	</div>

</body>
</html>