<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="userlist.css">
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/bootstrap.min.css" rel="stylesheet">
<title>ユーザー一覧</title>
</head>
<body>
	<header>
		<div class="alert alert-dark" role="alert">
			<p class="text-right">

				${sessionScope.userInfo.name} さん <a href="LogoutServlet"
					class="badge badge-logout">ログアウトする</a>
			</p>
		</div>
	</header>
	<div class="container">
		<h1 class=userlist1>ユーザー一覧</h1>

		<p class="text-right">
			<a class="btn btn-primary" href="ShinnkitourokuServlet">新規登録</a> <br>
			<br>
		<form action="UserListServlet" method="post">
			<div class="form-group row">
				<label for="inputuserID" class="col-sm-2 col-form-label">ログインID</label>
				<div class="col-sm-10">
					<input type="text" name="login_id" class="form-control"
						id="inputuserID">
				</div>
			</div>

			<div class="form-group row">
				<label for="inputusername" class="col-sm-2 col-form-label">ユーザー名</label>
				<div class="col-sm-10">
					<input type="text" name="name" class="form-control"
						id="inputusername">
				</div>
			</div>

			<div class="form-group row">
				<label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>
				<div class="row col-sm-10">
					<div class="col-sm-5">
						<input type="date" name="fromBirthDate">
					</div>
					～
					<div class="col-sm-1 text-center"></div>
					<div class="col-sm-5">
						<input type="date" name="toBirthDate">
					</div>
				</div>
			</div>

			<div class="form-group row">
				<div class="col-sm-10">
					<p class="text-right">
						<button type="submit" class="btn btn-outline-success">検索</button>
					</p>
				</div>
			</div>
		</form>



		<table class="table">
			<thead class="alert alert-primary" role="alert">
				<tr>
					<th scope="col">ログインID</th>
					<th scope="col">ユーザー名</th>
					<th scope="col">生年月日</th>
					<th scope="col"></th>
				</tr>
			</thead>
			<tbody>

				<c:forEach var="item" items="${list}">
					<tr>
							<!--todo 他のIDは管理者の参照はできないようにする→テーブルに管理者参照は表示されず。-->
							<!-- adminのテーブルの参照ボタンをなくす。adminでなければadminの参照がでないよ -->
							<td>${item.login_id}</td>
							<td>${item.name}</td>
							<td>${item.birth_date}</td>


							<td>
							
							<a class="btn btn-primary"href="UserDetailServlet?id=${item.id}" role="button">詳細 </a>
						
								<c:if test="${userInfo.login_id=='admin' || userInfo.login_id == item.login_id}">
									<a class="btn btn-danger"
										href="UserUpdateServlet?id=${item.id}" role="button">更新</a>

								</c:if> <c:if test="${userInfo.login_id=='admin'}">
									<a class="btn btn-info" href="UserDeleteServlet?id=${item.id}"
										role="button">削除</a>
								</c:if>

						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>

</body>
</html>
