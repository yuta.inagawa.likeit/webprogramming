<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/bootstrap.min.css" rel="stylesheet">
<title>田中太郎</title>
</head>
<body>
	<header>
		<div class="alert alert-dark" role="alert">
			<p class="text-right">
				${sessionScope.userInfo.name}さん <a href="LogoutServlet" class="badge badge-logout">ログアウトする</a>
			</p>
		</div>
	</header>
	<div class=container>
		<form>
			<h1>
				<p class="text-center">ユーザ情報詳細参照</p>
			</h1>
		</form>
		<form>
			<p class="text-left">ログインID</p>
			<p class="text-center">${user.login_id}</p>
			<br> <br>
			<p class="text-left">ユーザー名</p>
			<p class="text-center">${requestScope.user.name}</p>
			<br> <br>
			<p class="text-left">生年月日</p>
			<p class="text-center">${requestScope.user.birth_date}<br> <br>
			<p class="text-left">登録日時
			<p class="text-center">${requestScope.user.create_date}</p>
			<br> <br>
			<p class="text-left">更新日時</p>
			<p class="text-center">${requestScope.user.update_date}<br>
		</form>
		<form>
			<a href="UserListServlet" class="badge badge-back"><h4>戻る</h4></a>
		</form>
	</div>
</body>
</html>