<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/bootstrap.min.css" rel="stylesheet">
<title>新規登録画面</title>
</head>

<header>
	<div class="alert alert-dark" role="alert">
		<p class="text-right">
			${sessionScope.userInfo.name}さん <a href="LogoutServlet">ログアウトする</a>
		</p>
	</div>
</header>

<c:if test="${errmsg != null}">
	<p class="text-danger">${errmsg}</p>
</c:if>

<body>
	<h2>
		<p class="text-center">ユーザー新規登録</p>
	</h2>
	<br>
	<br>
	<div class="container">

		<form class=touroku method="post" action="ShinnkitourokuServlet">

			<div class="form-group row">
				<label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
				<div class="col-sm-10">
					<input type="text" name="login_id">
				</div>
			</div>

			<div class="form-group row">

				<label for="password" class="col-sm-2 col-form-label">パスワード</label>
				<div class="col-sm-10">
					<input type="password" name="password">
				</div>
			</div>
			<div class="form-group row">
				<label for="passwordConf" class="col-sm-2 col-form-label">パスワード(確認)</label>
				<div class="col-sm-10">
					<input type="password" name="passwordConf">
				</div>
			</div>

			<div class="form-group row">
				<label for="userName" class="col-sm-2 col-form-label">ユーザ名</label>
				<div class="col-sm-10">
					<input type="text" name="name">
				</div>
			</div>

			<div class="form-group row">
				<label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>
				<div class="col-sm-10">
					<input type="date" name="birth_date">
				</div>
			</div>

			<div class="submit-button-area">
				<button type="submit" class="btn btn-primary btn-lg btn-block">登録</button>
			</div>
		</form>

		<div class="col-xs-4">
			<a href="UserListServlet">戻る</a>
		</div>

	</div>
</body>
</html>